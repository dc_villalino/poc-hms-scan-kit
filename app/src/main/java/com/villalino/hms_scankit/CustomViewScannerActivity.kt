package com.villalino.hms_scankit

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.huawei.hms.hmsscankit.OnLightVisibleCallBack
import com.huawei.hms.hmsscankit.OnResultCallback
import com.huawei.hms.hmsscankit.RemoteView
import com.huawei.hms.hmsscankit.ScanUtil
import com.huawei.hms.ml.scan.HmsScan
import com.villalino.hms_scankit.databinding.ActivityCustomizedScanViewBinding

/**
 *
 * Author @Darren Christopherson Villalino
 */
class CustomViewScannerActivity : Activity() {
    //scan_view_finder default width & height is  300dp
    val SCAN_FRAME_SIZE = 300
    private lateinit var mBinding: ActivityCustomizedScanViewBinding
    var mScreenWidth = 0
    var mScreenHeight = 0
    private lateinit var remoteView: RemoteView
    private val img = intArrayOf(R.drawable.flashlight_on, R.drawable.flashlight_off)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_customized_scan_view)
        mBinding.executePendingBindings()
        initializeViews(savedInstanceState)
    }

    private fun initializeViews(savedInstanceState: Bundle?) {
        //1.get screen density to calculate viewfinder's rect
        val dm = resources.displayMetrics
        val density = dm.density
        //2.get screen size
        mScreenWidth = resources.displayMetrics.widthPixels
        mScreenHeight = resources.displayMetrics.heightPixels
        val scanFrameSize = (SCAN_FRAME_SIZE * density).toInt()
        //3.calculate viewfinder's rect,it's in the middle of the layout
        //set scanning area(Optional, rect can be null,If not configure,default is in the center of layout)
        val rect = Rect()
        rect.left = mScreenWidth / 2 - scanFrameSize / 2
        rect.right = mScreenWidth / 2 + scanFrameSize / 2
        rect.top = mScreenHeight / 2 - scanFrameSize / 2
        rect.bottom = mScreenHeight / 2 + scanFrameSize / 2
        //initialize RemoteView instance, and set calling back for scanning result
        remoteView = RemoteView.Builder().setContext(this).setBoundingBox(rect).setFormat(HmsScan.ALL_SCAN_TYPE).build()
        // When the light is dim, this API is called back to display the flashlight switch.
        remoteView.setOnLightVisibleCallback(OnLightVisibleCallBack { visible ->
            if (visible) {
                mBinding.flushBtn.visibility = View.VISIBLE
            }
        })
        remoteView.setOnResultCallback(OnResultCallback { result ->
            if (result != null && result.isNotEmpty() && result[0] != null && !TextUtils.isEmpty(result[0].getOriginalValue())) {
                val intent = Intent(this@CustomViewScannerActivity, DisplayQrCodeContent::class.java)
                intent.putExtra(ScanUtil.RESULT, result[0])
                startActivity(intent)
            }
        })
        // Load the customized view to the activity.
        remoteView.onCreate(savedInstanceState)
        val params = FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        mBinding.baseView.addView(remoteView, params)
        setFlashOperation()
        setBackOperation()
    }

    private fun setFlashOperation() {
        mBinding.flushBtn.setOnClickListener {
            if (remoteView.lightStatus) {
                remoteView.switchLight()
                mBinding.flushBtn.setImageResource(img[1])
            } else {
                remoteView.switchLight()
                mBinding.flushBtn.setImageResource(img[0])
            }
        }
    }

    private fun setBackOperation() {
        mBinding.ivBack.setOnClickListener { finish() }
    }

    override fun onStart() {
        super.onStart()
        remoteView.onStart()
    }

    override fun onResume() {
        super.onResume()
        remoteView.onResume()
    }

    override fun onPause() {
        super.onPause()
        remoteView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        remoteView.onDestroy()
    }

    override fun onStop() {
        super.onStop()
        remoteView.onStop()
    }
}