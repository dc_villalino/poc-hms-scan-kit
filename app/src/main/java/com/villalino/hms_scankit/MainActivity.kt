package com.villalino.hms_scankit

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.huawei.hms.hmsscankit.ScanUtil
import com.huawei.hms.ml.scan.HmsScan
import com.huawei.hms.ml.scan.HmsScanAnalyzerOptions
import com.villalino.hms_scankit.GenerateBarcodeActivity
import com.villalino.hms_scankit.databinding.ActivityMainBinding

/**
 * Author @Darren Christopherson Villalino
 */
class MainActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mBinding.executePendingBindings()
    }

    fun loadScanKitBtnClick(view: View?) {
        requestPermission(CAMERA_REQ_CODE, DECODE)
    }

    fun customizedViewBtnClick(view: View?) {
        requestPermission(DEFINED_CODE, DECODE)
    }

    fun generateQRCodeBtnClick(view: View?) {
        requestPermission(GENERATE_CODE, GENERATE)
    }

    private fun requestPermission(requestCode: Int, mode: Int) {
        if (mode == DECODE) {
            decodePermission(requestCode)
        } else if (mode == GENERATE) {
            generatePermission(requestCode)
        }
    }

    private fun decodePermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE),
                requestCode)
    }

    private fun generatePermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                requestCode)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == GENERATE_CODE) {
            val intent = Intent(this, GenerateBarcodeActivity::class.java)
            startActivity(intent)
        }
        if (grantResults.size < 2 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
            return
        }
        if (requestCode == CAMERA_REQ_CODE) {
            ScanUtil.startScan(this, REQUEST_CODE_SCAN_ONE, HmsScanAnalyzerOptions.Creator().create())
        }
        if (requestCode == DEFINED_CODE) {
            val intent = Intent(this, CustomViewScannerActivity::class.java)
            startActivity(intent)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK || data == null) {
            return
        }
        //DefaultView
        if (requestCode == REQUEST_CODE_SCAN_ONE) {
            val obj: HmsScan = data.getParcelableExtra(ScanUtil.RESULT)
            if (obj != null) {
                val intent = Intent(this, DisplayQrCodeContent::class.java)
                intent.putExtra(RESULT, obj)
                startActivity(intent)
            }
        }
    }

    companion object {
        const val DECODE = 1
        const val GENERATE = 2
        const val CAMERA_REQ_CODE = 111
        const val DEFINED_CODE = 222
        const val GENERATE_CODE = 666
        private const val REQUEST_CODE_SCAN_ONE = 0X01
        const val RESULT = "SCAN_RESULT"
    }
}