package com.villalino.hms_scankit.utils

class Constants {
    object SCAN_TYPE {
        const val QRCODE_SCAN_TYPE = "QRCODE"
        const val AZTEC_SCAN_TYPE = "AZTEC"
        const val DATAMATRIX_SCAN_TYPE = "DATAMATRIX"
        const val PDF417_SCAN_TYPE = "PDF417"
        const val CODE93_SCAN_TYPE = "CODE93"
        const val CODE39_SCAN_TYPE = "CODE39"
        const val CODE128_SCAN_TYPE = "CODE128"
        const val EAN13_SCAN_TYPE = "EAN13"
        const val EAN8_SCAN_TYPE = "EAN8"
        const val ITF14_SCAN_TYPE = "ITF14"
        const val UPCCODE_A_SCAN_TYPE = "UPCCODE_A"
        const val UPCCODE_E_SCAN_TYPE = "UPCCODE_E"
        const val CODABAR_SCAN_TYPE = "CODABAR"
    }
}