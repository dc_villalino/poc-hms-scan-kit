package com.villalino.hms_scankit.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.widget.Toast
import com.huawei.hms.hmsscankit.ScanUtil
import com.huawei.hms.hmsscankit.WriterException
import com.huawei.hms.ml.scan.HmsBuildBitmapOption

/**
 * Author @Darren Christopherson Villalino
 */
object HMSScanKitUtils {
    @JvmStatic
    fun generateBarcode(context: Context?, width: Int, height: Int, type: Int, content: String?): Bitmap? {
        var resultImage: Bitmap? = null
        resultImage = try { //generate barcode
            val options = HmsBuildBitmapOption.Creator()
                    .setBitmapMargin(1)
                    .setBitmapColor(Color.RED)
                    .setBitmapBackgroundColor(Color.WHITE).create()
            ScanUtil.buildBitmap(content, type, width, height, options)
        } catch (e: WriterException) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
            null
        }
        return resultImage
    }
}