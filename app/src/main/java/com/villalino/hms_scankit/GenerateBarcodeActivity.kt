package com.villalino.hms_scankit

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.databinding.DataBindingUtil
import com.huawei.hms.ml.scan.HmsScan
import com.villalino.hms_scankit.databinding.ActivityGenerateCodeBinding
import com.villalino.hms_scankit.utils.HMSScanKitUtils.generateBarcode

/**
 *
 * Author @Darren Christopherson Villalino
 */
class GenerateBarcodeActivity : Activity() {
    private lateinit var mBinding: ActivityGenerateCodeBinding
    private var type = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_generate_code)
        setupBarcodeType()
    }

    private fun setupBarcodeType() {
        mBinding.spBarcodeType.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                type = BARCODE_TYPES[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                type = BARCODE_TYPES[0]
            }
        }
    }

    fun generateCodeBtnClick(v: View?) {
        val inputWidth = mBinding.barcodeWidth.text.toString()
        val inputHeight = mBinding.barcodeWidth.text.toString()
        val height: Int
        val width: Int
        if (inputWidth.isEmpty() || inputHeight.isEmpty()) {
            width = 700
            height = 700
        } else {
            width = inputWidth.toInt()
            height = inputHeight.toInt()
        }
        if (mBinding.barcodeContent.text.toString().isNotEmpty()) {
            val generateBitmap = generateBarcode(this,
                    width, height,
                    type, mBinding.barcodeContent.text.toString())
                    ?: return
            mBinding.barcodeImage.setImageBitmap(generateBitmap)
        }
    }

    companion object {
        private val BARCODE_TYPES = intArrayOf(HmsScan.QRCODE_SCAN_TYPE, HmsScan.DATAMATRIX_SCAN_TYPE, HmsScan.PDF417_SCAN_TYPE, HmsScan.AZTEC_SCAN_TYPE,
                HmsScan.EAN8_SCAN_TYPE, HmsScan.EAN13_SCAN_TYPE, HmsScan.UPCCODE_A_SCAN_TYPE, HmsScan.UPCCODE_E_SCAN_TYPE, HmsScan.CODABAR_SCAN_TYPE,
                HmsScan.CODE39_SCAN_TYPE, HmsScan.CODE93_SCAN_TYPE, HmsScan.CODE128_SCAN_TYPE, HmsScan.ITF14_SCAN_TYPE)
    }
}