package com.villalino.hms_scankit

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.huawei.hms.hmsscankit.ScanUtil
import com.huawei.hms.ml.scan.HmsScan
import com.villalino.hms_scankit.databinding.ActivityDisplayQrContentBinding
import com.villalino.hms_scankit.utils.Constants

/**
 *
 * Author @Darren Christopherson Villalino
 */
class DisplayQrCodeContent : Activity() {
    private lateinit var mBinding: ActivityDisplayQrContentBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_display_qr_content)
        mBinding.executePendingBindings()

        if (intent != null) {
            val obj: HmsScan = intent.getParcelableExtra(ScanUtil.RESULT)
            valueFillIn(obj)
        } else
            finish()
    }

    //this method shows what are the supported  Scan Type
    //and also determines its Scan Type Form
    @SuppressLint("SetTextI18n")
    private fun valueFillIn(hmsScan: HmsScan) {
        mBinding.tvResultType.text = "Text"
        mBinding.tvRawValue.text = hmsScan.getOriginalValue()
        var codeFormat = ""
        when {
            hmsScan.getScanType() == HmsScan.QRCODE_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.QRCODE_SCAN_TYPE
                when {
                    hmsScan.getScanTypeForm() == HmsScan.PURE_TEXT_FORM -> {
                        mBinding.tvResultType.text = "Text"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.EVENT_INFO_FORM -> {
                        mBinding.tvResultType.text = "Event"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.CONTACT_DETAIL_FORM -> {
                        mBinding.tvResultType.text = "Contact"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.DRIVER_INFO_FORM -> {
                        mBinding.tvResultType.text = "License"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.EMAIL_CONTENT_FORM -> {
                        mBinding.tvResultType.text = "Email"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.LOCATION_COORDINATE_FORM -> {
                        mBinding.tvResultType.text = "Location"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.TEL_PHONE_NUMBER_FORM -> {
                        mBinding.tvResultType.text = "Tel"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.SMS_FORM -> {
                        mBinding.tvResultType.text = "SMS"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.WIFI_CONNECT_INFO_FORM -> {
                        mBinding.tvResultType.text = "Wi-Fi"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.URL_FORM -> {
                        mBinding.tvResultType.text = "WebSite"
                    }
                    else -> {
                        mBinding.tvResultType.text = "Text"
                    }
                }
            }
            hmsScan.getScanType() == HmsScan.AZTEC_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.AZTEC_SCAN_TYPE
            }
            hmsScan.getScanType() == HmsScan.DATAMATRIX_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.DATAMATRIX_SCAN_TYPE
            }
            hmsScan.getScanType() == HmsScan.PDF417_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.PDF417_SCAN_TYPE
            }
            hmsScan.getScanType() == HmsScan.CODE93_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.CODE93_SCAN_TYPE
            }
            hmsScan.getScanType() == HmsScan.CODE39_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.CODE39_SCAN_TYPE
            }
            hmsScan.getScanType() == HmsScan.CODE128_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.CODE128_SCAN_TYPE
            }
            hmsScan.getScanType() == HmsScan.EAN13_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.EAN13_SCAN_TYPE
                when {
                    hmsScan.getScanTypeForm() == HmsScan.ISBN_NUMBER_FORM -> {
                        mBinding.tvResultType.text = "ISBN"
                    }
                    hmsScan.getScanTypeForm() == HmsScan.ARTICLE_NUMBER_FORM -> {
                        mBinding.tvResultType.text = "Product"
                    }
                    else -> {
                        mBinding.tvResultType.text = "Text"
                    }
                }
            }
            hmsScan.getScanType() == HmsScan.EAN8_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.EAN8_SCAN_TYPE
                if (hmsScan.getScanTypeForm() == HmsScan.ARTICLE_NUMBER_FORM) {
                    mBinding.tvResultType.text = "Product"
                } else {
                    mBinding.tvResultType.text = "Text"
                }
            }
            hmsScan.getScanType() == HmsScan.ITF14_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.ITF14_SCAN_TYPE
            }
            hmsScan.getScanType() == HmsScan.UPCCODE_A_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.UPCCODE_A_SCAN_TYPE
                if (hmsScan.getScanTypeForm() == HmsScan.ARTICLE_NUMBER_FORM) {
                    mBinding.tvResultType.text = "Product"
                } else {
                    mBinding.tvResultType.text = "Text"
                }
            }
            hmsScan.getScanType() == HmsScan.UPCCODE_E_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.UPCCODE_E_SCAN_TYPE
                if (hmsScan.getScanTypeForm() == HmsScan.ARTICLE_NUMBER_FORM) {
                    mBinding.tvResultType.text = "Product"
                } else {
                    mBinding.tvResultType.text = "Text"
                }
            }
            hmsScan.getScanType() == HmsScan.CODABAR_SCAN_TYPE -> {
                codeFormat = Constants.SCAN_TYPE.CODABAR_SCAN_TYPE
            }
        }
        mBinding.tvCodeFormat.text = codeFormat
    }
}